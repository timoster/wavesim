function addWave() {
    var $e = $(`<div class="wave-container" data-id="${nextWaveId}"> \
        <canvas></canvas> \
        <div class="sliders"> \
            <table> \
                <tr><td>y&#770;</td><td><input type="range" min="-60" max="60" value="10" name="yHat" /></td></tr> \
                <tr><td>&lambda;</td><td><input type="range" min="1" max="400" value="200" name="lambda" /></td></tr> \
                <tr><td>&delta;</td><td><input type="range" min="0" max="4" step="0.01" value="0.3" name="damp" /></td></tr> \
            </table> \
        </div> \
        <div class="knobs"> \
            <table> \
                <tr><td>T</td><td><input type="number" class="span" min="0" max="20" step="0.01" value="2" /></td></tr> \
                <tr><td>Delay&emsp;</td><td><input type="number" class="delay" min="0" max="100" step="0.01" value="0" /></td></tr> \
                <tr><td>Right</td><td><input type="checkbox" class="right" /></td></tr> \
                <tr><td colspan="2"><button class="remove">Remove</button></td></tr> \
            </table> \
        </div> \
    </div>`);

    $("#waves").append($e);

    var container = genContainer();
    prepareCanvas(container, $e.find("canvas"));
    container._id = nextWaveId;
    container.wave = genWave();

    $e.find(".sliders tr").each(function() {
        $(this).append('<td><span class="preview"></span></td>');
        $(this).find(".preview").html($(this).find("input").val());
        eval(`container.wave.${$(this).find("input").attr("name")}=${parseFloat($(this).find("input").val())}`);
    });

    $e.find("input[type=range]").mousemove(function() {
        var val = $(this).val();
        $(this).closest("tr").find(".preview").html(val);
        eval(`container.wave.${$(this).attr("name")}=${parseFloat(val)}`);
    });

    $e.find(".remove").click(function() {
        var $container = $(this).closest(".wave-container");
        $container.remove();

        for (var i = 0; i < waves.length; i++) {
            if (waves[i].id == $container.data("id")) {
                waves.splice(i, 1);
                break;
            }
        }
    });

    container.wave.span = parseFloat($e.find(".span").val());
    container.wave.delay = parseFloat($e.find(".delay").val());
    container.wave.rtl = $e.find(".right").is(":checked");

    $e.find(".span").change(function() {
        container.wave.span = parseFloat($(this).val());
    });

    $e.find(".delay").change(function() {
        container.wave.delay = parseFloat($(this).val());
    });

    $e.find(".right").change(function() {
        container.wave.rtl = $(this).is(":checked");
    });

    waves.push(container);
    nextWaveId++;
}

$(document).ready(function() {
    $("#play").click(function() {
        togglePlay();
    });

    $("#stop").click(function() {
        stop();
    });

    $("#back").click(function() {
        if (!isPlaying) frames -= 10;
    });

    $("#forward").click(function() {
        if (!isPlaying) frames += 10;
    });

    $("#wave-detail").on("input", function() {
        detail = parseInt($(this).val());
    });

    $("#add-wave").click(addWave);
});
