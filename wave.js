var frames = 0;
var nextWaveId = 1;

var isPlaying = false;
var padding = 60;
var detail = 50;

var waves = [];

var waveContainer = {
    _id: 0,
    canvas: null,
    ctx: null,
    wave: null
};

var wave = {
    yHat: 0,
    span: 0,
    lambda: 0,
    damp: 0,
    delay: 0,
    rtl: false
};

var main = genContainer();
prepareCanvas(main, $("#main"));

function prepareCanvas(container, canvas) {
    var ctx = canvas[0].getContext('2d');
    var ratio = canvas.height() / canvas.width();
    canvas = canvas[0];
    canvas.width = 1080;
    canvas.height = canvas.width * ratio;
    container.canvas = canvas;
    container.ctx = ctx;
}

function updatePlay() {
    $("#play").html(isPlaying ? "Pause" : "Play");
}

function togglePlay() {
    isPlaying = !isPlaying;
    updatePlay();
}

function stop() {
    isPlaying = false;
    frames = 0;
    updatePlay();
}

function genContainer() {
    return Object.create(waveContainer);
}

function genWave() {
    return Object.create(wave);
}

function getWaveHeight(wave, x, t, w) {
    if (wave.rtl) x = w - x;
    if (t < wave.delay) return 0;
    t -= wave.delay;
    if (x / wave.lambda > t / wave.span) return 0;
    return wave.yHat * Math.sin(2 * Math.PI * ((t / wave.span) - (x / wave.lambda))) * Math.exp(-wave.damp * t);
}

function drawCanvas(container, allWaves) {
    var canvas = container.canvas;
    var ctx = container.ctx;

    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.strokeStyle = "white";

    ctx.beginPath();
    ctx.moveTo(padding, canvas.height / 2.0);
    ctx.lineTo(canvas.width - padding, canvas.height / 2.0);
    ctx.stroke();

    var w = canvas.width - 2 * padding;
    var r = Math.min(8, w / (detail * 2));

    for (var x = 0; x <= w + r; x += w / detail) {
        var y = 0;

        if (allWaves) {
            for (var i = 0; i < waves.length; i++) {
                y += getWaveHeight(waves[i].wave, x, frames / 60, w);
            }
        } else {
            y = getWaveHeight(container.wave, x, frames / 60, w);
        }

        ctx.beginPath();
        ctx.arc(x + padding, canvas.height / 2 - y, r, 0, 2 * Math.PI, false);
        ctx.stroke();
    }
}

(function draw() {
    if (isPlaying) frames += 1;
    $("#time").text(parseFloat(frames / 60).toFixed(2));

    for (var i = 0; i < waves.length; i++) {
        drawCanvas(waves[i], false);
    }

    drawCanvas(main, true);

    window.requestAnimationFrame(draw);
})();
